extends KinematicBody

class_name Enemy

signal spawn_mini_game(entity, type, list)

enum States {PATROL, ALERT, DANGER}
const FALL_ACCELERATION : int = 10
const MIN_ALERT_POINTS : int = 3
const MIN_DANGER_POINTS : int = 6
const MAX_SOUND_POINTS : int = 4
const MAX_VISUAL_POINTS : int = 6
const EXIT_DETECTION_TIME : int = 1

export(float) var speed = 1.5

var direction : Vector3 = Vector3.ZERO
var velocity : Vector3 = Vector3.ZERO
var visible_player : bool = false
var player_on_dangerzone : bool = false
var state = States.PATROL
var on_detection_cooldown : bool = false
var sound_detection_points : int = 0
var visual_detection_points : int = 0

onready var layer = global_transform.origin.z
onready var player_ref : KinematicBody = get_parent().get_node("Player") # TODO We will make a manager later
onready var ray_cast : RayCast = get_node("RayCast")

func _ready():
	$NearFOV.connect("body_entered", self, "_on_NearFOV_body_entered")
	$NearFOV.connect("body_exited", self, "_on_NearFOV_body_exited")
	$DistantFOV.connect("body_entered", self, "_on_DistantFOV_body_entered")
	$DistantFOV.connect("body_exited", self, "_on_DistantFOV_body_exited")
	$DangerZone.connect("body_entered", self, "_on_DangerZone_body_entered")
	$DangerZone.connect("body_exited", self, "_on_DangerZone_body_exited")
	$MiniGameZone.connect("body_entered", self, "_on_MiniGameZone_body_entered")
	$MiniGameZone.connect("body_exited", self, "_on_MiniGameZone_body_exited")
	$ExitDetectionTimer.connect("timeout", self, "_exit_detection_timer_timeout")
	$VisualDetectionTimer.connect("timeout", self, "_visual_detection_timer_timeout")
	$ClickArea.connect("input_event", self, "_click_area")


func _physics_process(delta):
	# if visible_player:
	#	if _check_raycast(player_ref.global_transform.origin):
	#		if state != States.DANGER:
	#			_enter_danger()
	
	match state:
		States.PATROL:
			$EnemySprite/SirenSprite.play("default")
			direction = _patrol_direction()
		States.ALERT:
			$EnemySprite/SirenSprite.play("alert")
			direction = _alert_direction()
		States.DANGER:
			$EnemySprite/SirenSprite.play("danger")
			direction = _danger_direction()
	
	velocity.x = direction.x * speed
	velocity.y -= FALL_ACCELERATION * delta
	velocity = move_and_slide(velocity, Vector3.UP)
	
	_fliph(direction.x)
	#_flipv(direction.y)


func _fliph(direction_x):
	if direction_x > 0:
		rotation_degrees.y = 0
	else:
		rotation_degrees.y = 180


func _flipv(direction_y):
	if direction_y > 0:
		rotation_degrees.x = 180
	else:
		rotation_degrees.x = 0


func _check_raycast(target : Vector3):
	ray_cast.set_cast_to(to_local(target))
	ray_cast.force_raycast_update()
	if ray_cast.is_colliding():
		return false
	else:
		return true


func _detection_points():
	return sound_detection_points + visual_detection_points


func add_sound_points(points):
	sound_detection_points = min(MAX_SOUND_POINTS, sound_detection_points + points)
	if points > 0:
		$ExitDectionTimer.start(EXIT_DETECTION_TIME)
		on_detection_cooldown = false
	_check_detection()

func add_visual_points(points):
	visual_detection_points = min(MAX_VISUAL_POINTS, visual_detection_points + points)
	if points > 0:
		$ExitDetectionTimer.start(EXIT_DETECTION_TIME)
		on_detection_cooldown = false
	_check_detection()


func _check_detection():
	var points = _detection_points()
	if points >= MIN_DANGER_POINTS and state != States.DANGER:
		_enter_danger()
	elif points >= MIN_ALERT_POINTS and state != States.ALERT:
		_enter_alert()
	elif points < MIN_ALERT_POINTS and state != States.PATROL:
		_enter_patrol()


func _on_MiniGameZone_body_entered(_body):
	_enable_mini_game()


func _on_MiniGameZone_body_exited(_body):
	_disable_mini_game()


func _on_NearFOV_body_entered(body):
	if _check_raycast(body.global_transform.origin):
		add_visual_points(MAX_VISUAL_POINTS)


func _on_NearFOV_body_exited(_body):
	ray_cast.set_cast_to(Vector3(0,0,0))
	ray_cast.force_raycast_update()
	#visible_player = false
	if state == States.DANGER and not player_on_dangerzone:
		$ExitDetectionTimer.start(EXIT_DETECTION_TIME)
		#_leave_danger()


func _on_DistantFOV_body_entered(body):
	# player_ref = body
	if _check_raycast(body.global_transform.origin):
		visible_player = true
		add_visual_points(1)


func _on_DistantFOV_body_exited(_body):
	visible_player = false
	pass


func _on_DangerZone_body_entered(_body):
	player_on_dangerzone = true
	add_visual_points(MAX_VISUAL_POINTS)
	#_enter_danger()


func _on_DangerZone_body_exited(_body):
	player_on_dangerzone = false
	if not visible_player:
		$ExitDetectionTimer.start(EXIT_DETECTION_TIME)
		#_leave_danger()


func _enter_danger():
	$MiniGameZone.monitoring = false
	$ExitDetectionTimer.stop()
	state = States.DANGER


#func _leave_danger():
#	_enter_alert()
#	$ExitDetectionTimer.start(EXIT_DETECTION_TIME)


func _exit_detection_timer_timeout():
	print("Diminui detecção")
	on_detection_cooldown = true
	
	if state == States.DANGER:
		add_visual_points(MIN_DANGER_POINTS - 1 - _detection_points())
	
	yield(get_tree().create_timer(5), "timeout")
	var points = _detection_points()
	for i in points:
		# If the enemy detects something, it stops the loop
		if on_detection_cooldown:
			if sound_detection_points:
				add_sound_points(-1)
			else:
				add_visual_points(-1)
			yield(get_tree().create_timer(1), "timeout")
		else:
			break


func _visual_timer_timeout():
	if _check_raycast(player_ref.position):
		add_visual_points(1)


func _alert_direction():
	pass


func _patrol_direction():
	pass


func _danger_direction():
	pass


func _enable_mini_game():
	$EnemySprite.modulate = Color(1, 0.5, 1, 1)
	$ClickArea/CollisionShapeClick.disabled = false


func _disable_mini_game():
	$EnemySprite.modulate = Color(1, 1, 1, 1)
	$ClickArea/CollisionShapeClick.disabled = true


func _click_area(_camera: Node, event: InputEvent, _position: Vector3,
				 _normal: Vector3, _shape_idx: int):
	if (event.is_action_pressed("click")):
		emit_signal("spawn_mini_game", self, "qte", ["qte_test"])


func _die():
	queue_free()


func _enter_patrol():
	state = States.PATROL


func _enter_alert():
	$MiniGameZone.monitoring = true
	state = States.ALERT


func mini_game_result(result : bool):
	if result:
		_die()
	else:
		print("Dano no jogador")
