extends Enemy

export(NodePath) var path1 : NodePath
export(NodePath) var path2 : NodePath
onready var pos1 : Vector3 = get_node(path1).global_transform.origin
onready var pos2 : Vector3 = get_node(path2).global_transform.origin
onready var current_patrol : Vector3 = pos1

func _process(_delta):
	if direction != Vector3.ZERO:
		$EnemySprite.play("run")
	else:
		$EnemySprite.play("idle")
	$Label3D.text = str(ceil(_detection_points()))


func _alert_direction():
	return _danger_direction()
	

func _patrol_direction():
	var dir : Vector3 = global_transform.origin.direction_to(current_patrol).normalized()
	if global_transform.origin.distance_to(pos1) < 0.5:
		current_patrol = pos2
		dir = global_transform.origin.direction_to(current_patrol).normalized()
	elif global_transform.origin.distance_to(pos2) < 0.5:
		current_patrol = pos1
		dir = global_transform.origin.direction_to(current_patrol).normalized()
	dir.z = layer
	return dir


func _danger_direction():
	var dir : Vector3 = Vector3(0,0,0)
	var player_pos : Vector3 = player_ref.global_transform.origin
	if global_transform.origin.distance_to(player_pos) > 0.1:
		dir = global_transform.origin.direction_to(player_pos).normalized()
	else:
		get_tree().reload_current_scene() ##########
	dir.z = layer
	return dir
