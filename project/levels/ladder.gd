extends Node

const Z_OFFSET : float = 0.3
var on_interactive_area = false
var on_top_exit = false
var on_bottom_exit = false
var enter_coord : Vector3

onready var player_ref : KinematicBody = get_parent().get_node("Player")

func _ready():
	enter_coord = Vector3(self.global_transform.origin.x, 0, -Z_OFFSET)

func _input(event):
	if player_ref.state == player_ref.States.USING_LADDER:
		#print("Usando escada")
		#print(event)
		if event.is_action("left"):
			player_ref.exit_ladder(Vector3(-0.5, 0, Z_OFFSET))
		elif event.is_action("right"):
			player_ref.exit_ladder(Vector3(0.5, 0, Z_OFFSET))
		elif event.is_action("up") and on_top_exit:
			player_ref.exit_ladder(Vector3(0, 0.5, Z_OFFSET))
		elif event.is_action("down") and on_bottom_exit:
			player_ref.exit_ladder(Vector3(0, -0.1, Z_OFFSET))
	
	elif ((event.is_action("up") and not on_top_exit) \
		or (event.is_action("down") and not on_bottom_exit)) \
		and on_interactive_area \
		and player_ref.state != player_ref.States.HOOKING:
		player_ref.enter_ladder(enter_coord)


func _on_InteractiveArea_body_exited(_body):
	on_interactive_area = false


func _on_InteractiveArea_body_entered(_body):
	on_interactive_area = true


func _on_BottomExitArea_body_entered(_body):
	on_bottom_exit = true


func _on_BottomExitArea_body_exited(_body):
	on_bottom_exit = false


func _on_TopExitArea_body_entered(_body):
	on_top_exit = true


func _on_TopExitArea_body_exited(_body):
	on_top_exit = false
