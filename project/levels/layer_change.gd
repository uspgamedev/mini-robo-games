extends Spatial

var enabled : bool = false
export(float) var front_layer_z
export(float) var back_layer_z
onready var front_layer : Spatial = get_node("FrontLayer")
onready var back_layer : Spatial = get_node("BackLayer")
onready var player_ref : KinematicBody = get_parent().get_node("Player") ######


func _ready():
	front_layer.global_transform.origin.z = front_layer_z
	back_layer.global_transform.origin.z = back_layer_z
	front_layer.connect("change_layer", self, "_change_layer")
	back_layer.connect("change_layer", self, "_change_layer")


func _change_layer(init_layer : Spatial):
	if $LockDownTimer.is_stopped():
		if front_layer == init_layer:
			player_ref.change_layer(front_layer.global_transform.origin, \
			back_layer.global_transform.origin)
		else:
			player_ref.change_layer(back_layer.global_transform.origin, \
			front_layer.global_transform.origin)
		_lock()


func _lock():
	$LockDownTimer.start(5)
	front_layer.lock()
	back_layer.lock()


func _unlock():
	front_layer.unlock()
	back_layer.unlock()
