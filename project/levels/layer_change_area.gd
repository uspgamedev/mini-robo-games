extends Spatial

signal change_layer(beg_layer)

var button
var enabled : bool = false


func _ready():
	if get_name().begins_with("Front"):
		button = "W"
	else:
		button = "S"
		
	$Torus.set_material($Torus.get_material().duplicate()) #######


func _on_InteractiveArea_body_entered(_body):
	enabled = true


func _on_InteractiveArea_body_exited(_body):
	enabled = false


func _input(event) -> void:
	if event.is_action_pressed(button) and enabled:
		emit_signal("change_layer", self)
	#### VERIFICAR INIMIGO

func lock():
	$Torus.get_material().albedo_color = Color(1, 0, 0)
	
func unlock():
	$Torus.get_material().albedo_color = Color(0, 1, 0)
