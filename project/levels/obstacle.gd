extends Node

enum enum_type { LADDER, OBSTACLE }
enum obs_enum { SHORT, MEDIUM, TALL }
export(obs_enum) var obs_type
export(enum_type) var type
var active = false
var peak_pos = Vector3()
var obs_time = { 
	obs_enum.SHORT : 1.0,
	obs_enum.MEDIUM : 2.0,
	obs_enum.TALL : 3.0,
}
onready var pos_a = $PositionA.global_transform.origin
onready var pos_b = $PositionB.global_transform.origin
onready var player_ref : KinematicBody = get_parent().get_node("Player") ######



func _ready() -> void:
	$InteractiveArea.connect("body_entered", self, "_on_InteractiveArea_body_entered")
	$InteractiveArea.connect("body_exited", self, "_on_InteractiveArea_body_exited")
	if type == enum_type.OBSTACLE:
		peak_pos = $PeakPosition.global_transform.origin


func _on_InteractiveArea_body_exited(_body):
	active = false


func _on_InteractiveArea_body_entered(_body):
	active = true


func _input(event):
	if (active and event.is_action_pressed("jump") and \
		player_ref.state != player_ref.States.HOOKING):
			
		active = false
		if (player_ref.global_transform.origin.distance_to(pos_a) < \
		player_ref.global_transform.origin.distance_to(pos_b)):
			yield(player_ref.use_obstacle(pos_a, peak_pos, pos_b, \
			obs_time[obs_type]), "completed")
		else:
			yield(player_ref.use_obstacle(pos_b, peak_pos, pos_a, \
			obs_time[obs_type]), "completed")
		active = true
