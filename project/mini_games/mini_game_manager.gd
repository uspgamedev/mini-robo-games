extends Control

const qte_path = "res://mini_games/scenes/qte/"
const puzzle_path = "res://mini_games/scenes/puzzle/"
var has_won


func _ready():
	for enemy in get_tree().get_nodes_in_group("Enemies"):
		enemy.connect("spawn_mini_game", self, "_spawn_mini_game")
	for puzzle in get_tree().get_nodes_in_group("Puzzles"):
		puzzle.connect("spawn_mini_game", self, "_spawn_mini_game")


func _spawn_mini_game(entity : Spatial, type : String, list : Array):
	if type == "qte":
		if yield(qte_mini_game(list), "completed"):
			entity.mini_game_result(true)
		else:
			entity.mini_game_result(false)
	else:
		if yield(puzzle_mini_game(entity, list), "completed"):
			entity.mini_game_result(true)
		else:
			entity.mini_game_result(false)


func qte_mini_game(list : Array):
	get_tree().paused = true
	
	var mini_name : String = list[randi() % list.size()]
	
	var mini_game : Control = \
	load(qte_path + mini_name + ".tscn").instance()
	add_child(mini_game)
	
	mini_game.connect("finish_mini_game", self, "_check_if_has_won")
	yield(mini_game, "finish_mini_game")
	
	get_tree().paused = false
	return has_won


func puzzle_mini_game(entity, list : Array):
	get_parent().get_node("Player").toggle_puzzle_camera()
	
	var mini_name : String = list[randi() % list.size()]
	
	var mini_game : Control = \
	load(puzzle_path + mini_name + ".tscn").instance()
	add_child(mini_game)
	
	mini_game.connect("finish_mini_game", self, "_check_if_has_won")
	entity.connect("quit", mini_game, "_finish_mini_game")
	yield(mini_game, "finish_mini_game")
	
	get_parent().get_node("Player").toggle_puzzle_camera()
	return has_won


func _check_if_has_won(has_won_arg: bool):
	has_won = has_won_arg
