extends Control

signal finish_mini_game(has_won)

var has_ended: bool = false


func _finish_mini_game(has_won: bool):
	if (not has_ended):
		emit_signal("finish_mini_game", has_won)
		queue_free()


func _on_X_pressed():
	_finish_mini_game(false)


func _on_Button_pressed():
	_finish_mini_game(true)
