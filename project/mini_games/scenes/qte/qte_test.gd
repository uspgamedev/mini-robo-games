extends Panel

signal finish_mini_game(has_won)

var has_ended: bool = false

func _process(_delta):
	var time_left : float = $Timer.time_left
	var seconds : String = str(floor(time_left))
	var miliseconds : String = str(floor(fmod(time_left*1000, 1000)))
	$TimerText.set_text(seconds + ":" + miliseconds + " LEFT!")

func _on_Button_pressed():
	_finish_mini_game(true)

func _on_Timer_timeout():
	_finish_mini_game(false)

func _finish_mini_game(has_won: bool):
	if (not has_ended):
		emit_signal("finish_mini_game", has_won)
		queue_free()
