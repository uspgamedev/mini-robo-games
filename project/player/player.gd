extends KinematicBody

enum States {DEFAULT, RUNNING, CROUCHING, HOOKING, USING_LADDER}

const NORMAL_SPEED : int = 3
const CROUCH_SPEED : int = 1
const RUN_SPEED : int = 5
const MAX_FALL_ACCELERATION : int = 10 # can be determined aby the level later?
const MAX_HOOK_HEIGHT : int = 3
const HOOK_DOWNTIME : int = 5

export var speed : float = NORMAL_SPEED

var state : int = States.DEFAULT
var puzzle_camera : bool = false

var speed_mult : float = 1
var fall_acceleration : int = MAX_FALL_ACCELERATION
var movement_enabled : bool = true
var hook_enabled : bool = true
var velocity : Vector3 = Vector3.ZERO
var direction : Vector3 = Vector3.ZERO
var old_direction : Vector3 = Vector3.ZERO
var enemies_nearby : Array = []

onready var ray_cast : RayCast = get_node("RayCast")


func _ready():
	$HookTimer.wait_time = HOOK_DOWNTIME


func _set_direction() -> Vector3:
	if not movement_enabled:
		return Vector3.ZERO
	
	direction = Vector3.ZERO
	
	if Input.is_action_pressed("right"):
		direction.x += 1
		$PlayerSprite.rotation_degrees.y = 0
	elif Input.is_action_pressed("left"):
		direction.x -= 1
		$PlayerSprite.rotation_degrees.y = 180
	elif Input.is_action_pressed("up"):
		direction.y += 1
	elif Input.is_action_pressed("down"):
		direction.y -= 1
	
	if state != States.USING_LADDER:
		direction.y = 0
		
	if direction != Vector3.ZERO:
		direction = direction.normalized()
	
	return direction


func _physics_process(delta) -> void:
	direction = _set_direction()
	velocity.x = direction.x * speed * speed_mult
	#print(velocity.y)
	if state == States.USING_LADDER:
		velocity.y = direction.y * speed * speed_mult
	else:
		velocity.y -= fall_acceleration * delta
	velocity = move_and_slide(velocity, Vector3.UP)

func _turn_gravity(on : bool):
	if on:
		fall_acceleration = MAX_FALL_ACCELERATION
	else:
		fall_acceleration = 0
		velocity.y = 0


func _process(_delta) -> void:
	$Label3D.text = str(ceil($HookTimer.time_left))
		
	if direction != Vector3.ZERO:
		if state == States.CROUCHING:
			$PlayerSprite.play("crouch_walk")
		else:
			$PlayerSprite.play("run")
	else:
		if state == States.CROUCHING:
			$PlayerSprite.play("crouch_idle")
		else:
			$PlayerSprite.play("idle")
	
	if movement_enabled:
		if Input.is_action_pressed("crouch"):
			if state != States.CROUCHING:
				toggle_crouch()
			
		elif Input.is_action_just_released("crouch"):
			toggle_crouch()
		
		if Input.is_action_pressed("run"):
			for enemy in enemies_nearby:
				enemy._enter_alert()
			if state != States.RUNNING:
				toggle_run()
			
		elif Input.is_action_just_released("run"):
			toggle_run()
	
	if state == States.USING_LADDER and direction.x != 0:
		state = States.DEFAULT
		_turn_gravity(true)


func toggle_run():
	if state == States.RUNNING:
		_update_speed(NORMAL_SPEED)
		state = States.DEFAULT
	else:
		_update_speed(RUN_SPEED)
		state = States.RUNNING


func toggle_crouch() -> void:
	if state == States.CROUCHING:
		$PlayerSprite.scale.y = 2
		$PlayerSprite.translation.y = 0
		$CollisionShape.shape.radius = 0.19
		$CollisionShape.translation.y = -0.154
		_update_speed(NORMAL_SPEED)
		state = States.DEFAULT
	else:
		$PlayerSprite.scale.y = 1.5
		$PlayerSprite.translation.y = -0.1
		$CollisionShape.shape.radius = 0.13
		$CollisionShape.translation.y = -0.2
		_update_speed(CROUCH_SPEED)
		state = States.CROUCHING


func _update_speed(new_speed : float):
	speed = new_speed


func toggle_puzzle_camera() -> void:
	if puzzle_camera:
		$Camera/AnimationPlayer.play("Zoom In")
		puzzle_camera = false
	else:
		$Camera/AnimationPlayer.play("Zoom Out")
		puzzle_camera = true


func _toggle_input(movement : bool = true, hook : bool = true) -> void:
	if movement:
		if state == States.CROUCHING:
			toggle_crouch()
		if state == States.RUNNING:
			toggle_run()
		movement_enabled = not movement_enabled
	
	if hook:
		hook_enabled = not hook_enabled


func enter_ladder(ladder_coord : Vector3) -> void:
	state = States.USING_LADDER
	var init_pos = self.global_transform.origin
	ladder_coord.y = init_pos.y
	
	_toggle_input()
	#### Tocar animação
	$Tween.interpolate_property(self, "global_transform:origin", \
	init_pos, ladder_coord, 0.2, \
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	#yield(get_tree().create_timer(0.2), "timeout")
	_toggle_input()
	

func exit_ladder(exit_dir : Vector3) -> void:
	#print("Chamou")
	state = States.DEFAULT
	var init_pos = self.global_transform.origin
	
	_toggle_input()
	#### Tocar animação
	$Tween.interpolate_property(self, "global_transform:origin", \
	init_pos, init_pos + exit_dir, 0.3, \
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	_toggle_input()


func use_obstacle(init_pos : Vector3, peak_pos : Vector3, final_pos : Vector3, time : float):
	_toggle_input()
	
	time = time / speed_mult
	
	$Tween.interpolate_property(self, "global_transform:origin", \
	self.global_transform.origin, init_pos, 0.2, \
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	
	$Tween.interpolate_property(self, "global_transform:origin", \
	init_pos, peak_pos, time/2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	
	$Tween.interpolate_property(self, "global_transform:origin", \
	peak_pos, final_pos, time/2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	
	_toggle_input()


func change_layer(init_pos : Vector3, final_pos : Vector3):
	_toggle_input()
	
	$Tween.interpolate_property(self, "global_transform:origin", \
	self.global_transform.origin, init_pos, 0.2, \
	Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	
	var time : float = abs((final_pos.z - init_pos.z)*1) / speed_mult
	$Tween.interpolate_property(self, "global_transform:origin", \
	init_pos, final_pos, time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	
	_toggle_input()


func _use_hook():
	ray_cast.set_cast_to(MAX_HOOK_HEIGHT * Vector3.UP)
	ray_cast.force_raycast_update()
		
	if ray_cast.is_colliding():
			
		_turn_gravity(false)
		_toggle_input(true, false)
		state = States.HOOKING
		var final_pos : Vector3 = ray_cast.get_collision_point() - \
								Vector3(0, $CollisionShape.shape.radius, 0)
		var init_pos : Vector3 = self.global_transform.origin
		var time : float = abs((final_pos.y - init_pos.y)*0.2) / speed_mult
		$Tween.interpolate_property(self, "global_transform:origin", \
		init_pos, final_pos, time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween.start()
		
		yield($Tween, "tween_completed")
	else:
		print("Não tem o que o gancho acertar")


func _input(event):
	if hook_enabled:
		if event.is_action_pressed("hook") and $HookTimer.is_stopped():
			_use_hook()
		elif event.is_action_released("hook") and state == States.HOOKING:
			$HookTimer.start(HOOK_DOWNTIME)
			$Label3D.text = str(HOOK_DOWNTIME)
			
			_turn_gravity(true)
			_toggle_input(true, false)
			state = States.DEFAULT
			$Tween.stop(self)


func _on_EnemyDetectionArea_body_entered(body):
	enemies_nearby.append(body)


func _on_EnemyDetectionArea_body_exited(body):
	enemies_nearby.erase(body)

