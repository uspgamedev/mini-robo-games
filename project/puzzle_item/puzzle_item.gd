extends Spatial

signal spawn_mini_game(entity, type, list)
signal quit(quit_arg)
export(Array) var mini_game_list


func _on_PuzzleArea_body_exited(_body):
	emit_signal("quit", false)
	$ClickArea/CollisionShape.disabled = true


func _on_PuzzleArea_body_entered(_body):
	$ClickArea/CollisionShape.disabled = false


func _on_ClickArea_input_event(_camera: Node, event: InputEvent, _position: Vector3,
				 _normal: Vector3, _shape_idx: int):
	if (event.is_action_pressed("click")):
		emit_signal("spawn_mini_game", self, "puzzle", mini_game_list)


func mini_game_result(result : bool):
	if result:
		print("Puzzle bem sucedido")
	else:
		print("Puzzle mal sucedido")
